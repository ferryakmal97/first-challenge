// Custom Navigation Drawer / Sidebar with Image and Icon in Menu Options
// https://aboutreact.com/custom-navigation-drawer-sidebar-with-image-and-icon-in-menu-options/

import React from 'react';
import {
  SafeAreaView,
  View,
  StyleSheet,
  Image,
  Text,
  Linking,
} from 'react-native';

import {
  DrawerContentScrollView,
  DrawerItemList,
  DrawerItem,
} from '@react-navigation/drawer';

const CustomSidebarMenu = (props) => {

  return (
    <SafeAreaView style={{flex: 1}}>
      {/*Top Large Image */}
      <View style={{height: 150, backgroundColor: '#fff4e6', alignItems: 'center', justifyContent: 'center'}}>
        <Image source={require('./assets/ME.jpg')} style={{height: 100, width:100, borderRadius: 200/ 2}}></Image>
      </View>
      <View style={{height: 75, backgroundColor: '#be9b7b', alignItems: 'center', justifyContent: 'center'}}>
        <Text style={{color:'#fff4e6', fontSize:25, fontWeight:'bold'}}>Akmal Ghaffari</Text>
        <Text style={{color:'#fff4e6', fontSize:15, fontWeight:'bold'}}>FIFA PLAYER</Text>
      </View>
      <DrawerContentScrollView {...props}>
        <DrawerItemList {...props} />
      </DrawerContentScrollView>
      <Text
        style={{
          fontSize: 16,
          textAlign: 'center',
          color: 'grey'
        }}>
        www.singaraja.com
      </Text>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  sideMenuProfileIcon: {
    resizeMode: 'center',
    width: 100,
    height: 100,
    borderRadius: 100 / 2,
    alignSelf: 'center',
  },
  iconStyle: {
    width: 15,
    height: 15,
    marginHorizontal: 5,
  },
  customItem: {
    padding: 16,
    flexDirection: 'row',
    alignItems: 'center',
  },
});

export default CustomSidebarMenu;